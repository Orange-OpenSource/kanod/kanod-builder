#!/bin/bash

#  Copyright (C) 2020-2021 Orange
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

set -eu

for var in KANOD_BUILDER_REF KANOD_NODE_REF KANOD_GOGS_REF KANOD_VAULT_REF KANOD_TPM_REF KANOD_COMMON_REF GIT_URL TARGET_BUILDER_OS
do
    if [ -z "${!var}" ]; then
        echo "${var} must be defined"
        exit
    fi
done


build_package() {
    # $1 : kanod git project name
    # $2 kanod git project tag
    # $2 package name prefix
    echo "build_package with $1 $2 $3"
    if ! [ -d "./build/${1}" ]; then
        echo "Creating ${1} package"
        git clone "${GIT_URL}/${1}" "./build/${1}"
        (
            cd "./build/${1}"
            git checkout "${2}"
            ${PYTHON:-/usr/bin/python3.8} setup.py sdist &> "../${1}.log"
            # TODO : handle tag with prefix ("v0.2.4")
            mv dist/"${3}"-*.tar.gz ../../kanod-python-packages/"${3}-${2}".tar.gz
        )
    fi
}

export IMAGE_VERSION=${VERSION:-$KANOD_DISKIMAGE_LAUNCHER_REF}
export CONTAINER="kanod-diskimage-launcher/${TARGET_BUILDER_OS}"

envsubst \$IMAGE_VERSION < ./kanod-diskimage-launcher > ./kanod-diskimage-launcher.updated
INSTALL_DIR=${INSTALL_DIR:-$HOME}
mkdir -p "${INSTALL_DIR}"/.local/bin
cp ./kanod-diskimage-launcher.updated "${INSTALL_DIR}"/.local/bin/kanod-diskimage-launcher
chmod +x "${INSTALL_DIR}"/.local/bin/kanod-diskimage-launcher

if [ -n "${TOOLING_REGISTRY:-}" ]; then
    echo "Checking if $TOOLING_REGISTRY/${CONTAINER}:$IMAGE_VERSION exists"
    if docker pull "$TOOLING_REGISTRY/${CONTAINER}:$IMAGE_VERSION" &> /dev/null; then
        echo "Image $TOOLING_REGISTRY/${CONTAINER}:$IMAGE_VERSION already exists"
        exit 0
    fi
else
    echo "Checking if ${CONTAINER}:$IMAGE_VERSION exists"
    if docker image inspect "${CONTAINER}:$IMAGE_VERSION" &> /dev/null; then
        echo "Image ${CONTAINER}:$IMAGE_VERSION already exists"
        exit 0
    fi
fi

declare -a proxy_args
if [ -n "${http_proxy:-}" ]; then
    proxy_args+=(--build-arg "http_proxy=${http_proxy}")
    proxy_args+=(--build-arg "HTTP_PROXY=${http_proxy}")
fi
if [ -n "${https_proxy:-}" ]; then
    proxy_args+=(--build-arg "https_proxy=${https_proxy}")
    proxy_args+=(--build-arg "HTTPS_PROXY=${https_proxy}")
fi
if [ -n "${no_proxy:-}" ]; then
    proxy_args+=(--build-arg "no_proxy=${no_proxy}")
    proxy_args+=(--build-arg "NO_PROXY=${no_proxy}")
fi

declare -a env_args
env_args+=(--build-arg "KANOD_BUILDER_REF=${KANOD_BUILDER_REF}")
env_args+=(--build-arg "KANOD_NODE_REF=${KANOD_NODE_REF}")
env_args+=(--build-arg "KANOD_GOGS_REF=${KANOD_GOGS_REF}")
env_args+=(--build-arg "KANOD_VAULT_REF=${KANOD_VAULT_REF}")
env_args+=(--build-arg "KANOD_TPM_REF=${KANOD_TPM_REF}")
env_args+=(--build-arg "KANOD_COMMON_REF=${KANOD_COMMON_REF}")
mkdir -p build
mkdir -p kanod-python-packages

build_package image-builder "${KANOD_BUILDER_REF}" kanod-image-builder
build_package kanod-node "${KANOD_NODE_REF}" kanod-node
build_package gogs-service.git "${KANOD_GOGS_REF}" kanod-gogs
build_package vault-service.git "${KANOD_VAULT_REF}" kanod-vault
build_package tpm-registrar.git "${KANOD_TPM_REF}" kanod-registrar
build_package common-services.git "${KANOD_COMMON_REF}" kanod-common

echo "Python packages created:"
ls -1 kanod-python-packages

env_args+=(--build-arg "DIB_OPENSUSE_MIRROR=${DIB_OPENSUSE_MIRROR:-}")

if [ -n "${KANOD_PACKAGES_ONLY:-}" ]; then
    exit 0
fi

if [ -n "${TOOLING_REGISTRY:-}" ]; then
    docker build "${proxy_args[@]}" "${env_args[@]}" -f "${TARGET_BUILDER_OS}/Dockerfile" . -t "$TOOLING_REGISTRY/${CONTAINER}:$IMAGE_VERSION"
    echo "${NEXUS_KANOD_PASSWORD}" | docker login -u "${NEXUS_KANOD_USER}" --password-stdin "${TOOLING_REGISTRY}"
    docker push "$TOOLING_REGISTRY/${CONTAINER}:$IMAGE_VERSION"
else
    docker build "${proxy_args[@]}" "${env_args[@]}" -f "${TARGET_BUILDER_OS}/Dockerfile" . -t "${CONTAINER}:$IMAGE_VERSION"
fi

# shellcheck disable=SC2154

if [ "${KANOD_PRUNE_IMAGES:-0}" == "1" ]; then
    docker image prune -a --force --filter 'label=project=kanod-diskimage-launcher-${TARGET_BUILDER_OS}'
fi
