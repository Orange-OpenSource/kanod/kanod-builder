# Kanod Diskimage Launcher
Container images with Openstack ```diskimage-builder``` and kanod ```image-builder``` for Ubuntu, Centos and Opensuse.
## Container images build

Mandatory environment variables for the build process:

* `GIT_URL` : url of the GIT kanod group
* `KANOD_BUILDER_REF` : tag of the `image-builder` project
* `KANOD_NODE_REF` : tag of the `kanod-node` project
* `KANOD_GOGS_REF` : tag of the `gogs-service` project
* `KANOD_VAULT_REF` : tag of the `vault-service` project
* `KANOD_TPM_REF` : tag of the `tpm-registrar` project
* `KANOD_COMMON_REF` : tag of the `common-services` project
* `VERSION` : tag of the built images

The build process is called with : `./build.sh`

Containers images are built for these 3 OS families: Ubuntu(bionic), Centos(stream8) and Opensuse(Leap15.4).

The names of the container images are :

* kanod-diskimage-launcher/ubuntu
* kanod-diskimage-launcher/centos
* kanod-diskimage-launcher/opensuse

These images are tagged with the `${VERSION}` value.

The script `kanod-diskimage-launcher` is installed in the `${HOME}/.local/bin` directory and will be used for launching these containers.

## Usage

Mandatory environment variables for the launch process:

* `BUILDER_OS` : os version of the container (available values : `ubuntu`, `centos` and `opensuse`)
* `IMAGE_VERSION` : tag of the container
* `GIT_URL` : url of the kanod git group; this variable is mandatory for the build of common-services OS image

Optional environment variables for the launch process:

* `DIB_UBUNTU_MIRROR` : mirror for ubuntu packages
* `DIB_OPENSUSE_MIRROR` : mirror for opensuse packages
* `DIB_REGISTRY_MIRROR` : mirror for docker images
* `DIB_PYPI_MIRROR_URL`: mirror for python pip packages

For building OS images, the script `kanod-diskimage-launcher` is launched with the same arguments as the kanod-image-builder utility (cf image-builder project).

For example, a typical call will be : `kanod-diskimage-launcher <kanod_os_config> -s 'key=value'`

`kanod_os_config` is the name of the kanod configuration used to build the os. 
Authorized values for this name are : `kanod_node`, `kanod_common`, `kanod_gogs`, `kanod_registrar` and `kanod_vault`.
