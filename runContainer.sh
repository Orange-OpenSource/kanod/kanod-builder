#!/bin/bash

# shellcheck disable=SC2154

# update /etc/environment file for opensuse
if  [ -n "$http_proxy" ] ; then 
  echo "http_proxy=$http_proxy" >> /etc/environment
fi
if [ -n "$HTTP_PROXY" ] ; then 
  echo "HTTP_PROXY=$HTTP_PROXY" >> /etc/environment
fi
if [ -n "$https_proxy" ] ; then 
  echo "https_proxy=$https_proxy" >> /etc/environment
fi
if [ -n "$HTTPS_PROXY" ] ; then 
  echo "HTTPS_PROXY=$HTTPS_PROXY" >> /etc/environment
fi
if [ -n "$no_proxy" ] ; then 
  echo "no_proxy=$no_proxy" >> /etc/environment
fi
if [ -n "$NO_PROXY" ] ; then 
  echo "NO_PROXY=$NO_PROXY" >> /etc/environment
fi

# export KANOD_IMAGE_DEBUG=kanod_image_debug.txt

echo "running kanod-image-builder" "$@"
kanod-image-builder "$@"

rm -fr ./*.d
