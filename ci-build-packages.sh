#!/bin/bash

# shellcheck disable=SC2129

ROOT_DIR=$(readlink -f "$(dirname "${BASH_SOURCE[0]}")")

GIT_URL="$(git remote get-url origin | sed 's/\/kanod-diskimage-launcher.git//g')"
export GIT_URL
TARGET_BUILDER_OS=ubuntu
export TARGET_BUILDER_OS

KIAB_ENV="${ROOT_DIR}/kiab_conf"
git clone "${GIT_URL}"/kanod-in-a-bottle
(
    cd kanod-in-a-bottle || exit
    echo "KANOD_BUILDER_REF=$(yq e '.kanod-builder' templates/versions.yaml)" > "${KIAB_ENV}"
    echo "KANOD_COMMON_REF=$(yq e '.common' templates/versions.yaml)" >> "${KIAB_ENV}"
    echo "KANOD_GOGS_REF=$(yq e '.gogs' templates/versions.yaml)" >> "${KIAB_ENV}"
    echo "KANOD_VAULT_REF=$(yq e '.vault' templates/versions.yaml)" >> "${KIAB_ENV}"
    echo "KANOD_NODE_REF=$(yq e '.node' templates/versions.yaml)" >> "${KIAB_ENV}"
    echo "KANOD_TPM_REF=$(yq e '.tpm' templates/versions.yaml)" >> "${KIAB_ENV}"
    cd .. || exit
)

set -a
# shellcheck disable=SC1090,SC1091
source "${KIAB_ENV}"
set +a

KANOD_PACKAGES_ONLY=1 "${ROOT_DIR}"/build.sh
ls -l build
ls -l kanod-python-packages
